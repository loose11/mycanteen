package mycanteen
    
import(
 "net/http"
 "html/template"
 "strconv"
 "fmt"
 "appengine"
 "appengine/datastore"
 "time"
 "log"
 "strings"
)
 
type Food struct{
 Id int64
 Course string
 Name string 
 Date string
 Price float64
}
    
// No main function. Go App Engine use the init-func
func init() {
    http.HandleFunc("/", root)
    http.HandleFunc("/admin", admin)
    http.HandleFunc("/save", save)
	http.HandleFunc("/show", show)
	http.HandleFunc("/delete", deleteEntry)
}
    
func root(w http.ResponseWriter, r *http.Request) {    
    // Here we will add the Userpanel, but not know 
}
  
/*********************ADMINPANEL HANDLER**************************/
func admin(w http.ResponseWriter, r *http.Request) {      
    // Execute the parsing. We pass the ResponseWriter and a second value, which should be fill the gaps at the template. 
    // We have no gaps, so we have nothing to fill in. 
    if err := adminPanelTemplate.Execute(w, ""); err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
    }
}
/********************SAVE/PERSIST HANDLER************************/
func save(w http.ResponseWriter, r *http.Request) {    
 
 
    // You have to parse the form from our admin panel. It's mandatory!
    if err := r.ParseForm(); err != nil {
     fmt.Fprint(w,err) 
    }
      
    // Now we a readey to get the fish ;-)
    course := r.FormValue("course")
    name := r.FormValue("name")
    date := r.FormValue("date")
    // The form is string based, for later purpose we parse it to float
    price, _ := strconv.ParseFloat(r.FormValue("price"), 64)
    t := time.Now()
    id:= t.Unix()
    f := Food{id,course,name,date,price,}
    // Persist to Datastore
    // At this moment we cant save the values, hence we setup only this output to check the values.
    if  saveToDatastore(f,r) != true{
     http.Error(w, "Error until persist the meal", http.StatusInternalServerError)
    }
 
 	if err := saveTemplate.Execute(w,nil);err != nil{
  		http.Error(w, err.Error(), http.StatusInternalServerError)
	 }
	 
}

func show(w http.ResponseWriter, r *http.Request){

  	allMeals := getAllEntries(r)

	if err := entriesTemplate.Execute(w,allMeals);err != nil{
  		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	
}

func deleteEntry(w http.ResponseWriter, r *http.Request) {
	//c := appengine.NewContext(r)	

    if err := r.ParseForm(); err != nil {
     fmt.Fprint(w,err) 
    }

	

	ids := r.FormValue("id")
	log.Println(ids)
	selectedIds := strings.Split(ids,",")
	log.Println(selectedIds)
	deleteItems := deleteEntries(selectedIds , r)
	log.Printf("You delete %d datasets", deleteItems)

      
}

func deleteEntries(mealsID []string, r *http.Request) int{
	// Get context from 
    c := appengine.NewContext(r);

	
	for _,id := range mealsID{
		ID,_ := strconv.Atoi(id)
		q:= datastore.NewQuery("Meal").Ancestor(mealStoreKey(c)).Filter("Id =", ID ).KeysOnly()
		keys, err := q.GetAll(c, nil)
		if err != nil{
			return 0
		}
		log.Printf("ID: %v ", id)
		log.Printf("Keys: %v ", keys)
		e := datastore.DeleteMulti(c, keys)
		if e != nil{
			log.Printf("%v ", e)
			return 33		
		}		
	}

	return len(mealsID)
}
 
func saveToDatastore(f Food, r *http.Request) bool{
 // Get context from 
    c := appengine.NewContext(r);
 
    key := datastore.NewIncompleteKey(c, "Meal", mealStoreKey(c))
    _, err := datastore.Put(c, key, &f)
    if err != nil{
 return false
    }else{
 return true
    }
}
 
/******************Fetch all Entries from the datastore********************/
func getAllEntries(r *http.Request) []*Food{
 var q *datastore.Query
 var allMeals []*Food
 
 c := appengine.NewContext(r);
 // Query for all entries in the datastore with the meal store key
 q = datastore.NewQuery("Meal").Ancestor(mealStoreKey(c))
 if _, err := q.GetAll(c, &allMeals); err != nil {  
        return allMeals
    }
 
    return allMeals
}
 
// foodKey returns the key used for all food entries.
func mealStoreKey(c appengine.Context) *datastore.Key {
    // The string "default_food" here could be varied to have multiple cantines.
    return datastore.NewKey(c, "MealStore", "meal", 0, nil)
}
  
// Parse the HTMLTemplate, despite it is not neccesary yet.
var adminPanelTemplate = template.Must(template.New("adminPanelHTML").Parse(adminPanelHTML))
var saveTemplate = template.Must(template.New("saveHTML").Parse(saveHTML))
var entriesTemplate = template.Must(template.New("entriesHTML").Parse(entriesHTML))
    
// Here you define the HTML which will be parsed.
const adminPanelHTML= `
<html>
<head>
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<script src="js/adminScript.js"></script>
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
</head>
<body>
<form action="/save" id="saveMeal">
<select name="course">
     <option value="starter">starter</option>
    <option value="entree">entree</option>
    <option value="dessert">dessert</option>
   </select>
   
   Name: <input id="name" name="name" type="text" placeholder="Name">
   
   Price: <input name="price" type="text" placeholder="Price">
   
   Date: <input id="datepicker" name="date" type="text" placeholder="eg. 12/12/2012">
   <input id="saveMealButton" type="submit" value="Save">
  </form>
  <form action="/show" id="showMeal">
	<input id="showMealButton" type="submit" value="Show">
  </form>
  <form action="/delete" id="deleteEntry">
	<input id="deleteSelectedEntry" type="submit" value="Delete selected">
  </form>
  <div id="status"></div>
  <div id="result"></div>
</body>
</html>
` 
const saveHTML = `<div id="content">Saved to Datastore!</div>` 
const entriesHTML =  `{{range .}}<div id="content">{{.Date}} {{.Course}}:&#09;{{.Name}} costs {{.Price}}&#36;<input class="chk" type="checkbox" name="checkbox" value="{{.Id}}"/></div></br>{{end}}`

