$( document ).ready(function() {
	$("#saveMeal").submit(function(event){
		
			event.preventDefault();
			
    		var formData = $("#saveMeal").serializeArray();
				var URL = $("#saveMeal").attr("action");
			
				$.post( 
	             URL,
	             formData,
	             function(data) {
	                $('#status').html($(data).filter("#content"));
	             });	
    		
    });	
	$("#showMeal").submit(function(event){
		event.preventDefault();

		var URL = $("#showMeal").attr("action");
		$.post(
			URL,
			function(data){
				$('#result').html($(data).filter("#content"));			
			});
	});	

	$("#deleteEntry").submit(function(event){

		event.preventDefault();
		
		var chkArray = [];
		var URL = $("#deleteEntry").attr("action")

		$(".chk:checked").each(function(){
			chkArray.push($(this).val());
		});

		var selected = "id=";
		for (var i=0;i<chkArray.length;i++){
			
			selected +=  chkArray[i] + ",";
		}
		

		$.post(
			URL,
			selected,
			function(data){
				$('#status').html($(data).filter("#content"));
		});
	});		

});

    
